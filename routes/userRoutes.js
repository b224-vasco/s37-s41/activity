const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");



// Routes

// Route for checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)) 
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// // Route for getting user details using ID
// router.post("/details", (req, res) => {
// 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
// });

// S38 Activity Solution
// Retrieve user details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization);

	userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
});

// Route for enrolling an authenticated user
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}
	
	console.log(userData)
	if(userData.isAdmin == false){
		userController.enroll(data).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}

})


module.exports = router;